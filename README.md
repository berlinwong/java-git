# JavaGit

#### 介绍
#### 软件架构



#### 安装教程

1. fork的代码进行同步更新：

   `git remote -v `#查看当前项目的远程仓库配置

   `git remote add upstream 原始项目仓库的git地址` # 把原项目的远程仓库添加到fork的代码的远程中

   `git remote -v `# 可以看到原项目的远程仓库已经在配置里了

   `git fetch upstream` # 拉取最新的代码

   `git merge upstream/master` # mege合并

#### 使用说明

1.  克隆 git clone https://gitee.com/berlinwong/java-git.git

#### 参与贡献

1、克隆项目，添加代码
在`github`或者`gitee`上`fork`了自己感兴趣的项目，则会将原项目仓库复刻到自己的仓库中。
`git clone`自己的仓库
`git add *&git commit -m ""`进行修改项目内容，添加并提交
`git push -u origin master`将新的内容更新提交到远程仓库
在`github`或者`gitee`上新建`pull requests`，就是将你的修改申请提交到原始项目
等待管理员审核通过即可将你的修改合并至原始项目中，这样你的贡献就算达到了。



#### 特技


####  说明
其中TecherCode Fork了讲师的代码 如果需要在本项目中直接pull 可以先切换到 TeacherCode 目录
接着`git clone git@gitee.com:jayliu007/java-se282.git`
以后直接pull就可以了