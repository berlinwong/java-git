# Git的使用

## 1.什么是Git

- Git 是一个开源的分布式版本控制系统，用于敏捷高效地处理任何或小或大的项目。
- Git 是 Linus Torvalds 为了帮助管理 Linux 内核开发而开发的一个开放源码的版本控制软件。
- Git 与常用的版本控制工具 CVS, Subversion 等不同，它采用了分布式版本库的方式，不需要服务器端软件支持。

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120203544090.png" alt="image-20201120203544090" style="zoom:80%;" />

## 2.Git 工作流程

Git一般工作流程如下：

- 克隆 Git 资源作为工作目录。
- 在克隆的资源上添加或修改文件。
- 如果其他人修改了，你可以更新资源。
- 在提交前查看修改。
- 提交修改。
- 在修改完成后，如果发现错误，可以撤回提交并再次修改并提交。

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120203708116.png" alt="image-20201120203708116" style="zoom:67%;" />

## 3.Git 工作区、暂存区和版本库

- **工作区：**就是在电脑里相应的工作目录。
- **暂存区：**英文叫 stage 或 index。一般存放在 **.git** 目录下的 index 文件（.git/index）中，所以我们把暂存区有时也叫作索引（index）。
- **版本库：**工作区有一个隐藏目录 **.git**，这个不算工作区，而是 Git 的版本库（仓库），用与管理代码的版本。

![image-20201120204127513](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120204127513.png)Git仓库，分为本地仓库与远程仓库。本地存储的，包含.git的文件夹，称为本地仓库。与之对应的一个保存在远程服务器上的文件夹，叫做远程仓库。

![image-20201120210656032](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120210656032.png)

当文件位于不同的区域时，分别对应着不同的状态：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120210913247.png" alt="image-20201120210913247" style="zoom: 67%;" />

- **Untracked**: 未跟踪, 此文件在文件夹中, 但并没有加入到git库, 不参与版本控制. 通过`git add` 状态变为`Staged`
- **Unmodify**: 文件已经入库, 未修改, 即版本库中的文件快照内容与文件夹中完全一致. 这种类型的文件有两种去处, 如果它被修改, 而变为`Modified`. 如果使用`git rm`移出版本库, 则成为`Untracked`文件
- **Modified**: 文件已修改, 仅仅是修改, 并没有进行其他的操作. 这个文件也有两个去处, 通过`git add`可进入暂存`staged`状态, 使用`git checkout` 则丢弃修改过, 返回到`unmodify`状态, 这个`git checkout`即从库中取出文件, 覆盖当前修改
- **Staged**: 暂存状态. 执行`git commit`则将修改同步到库中, 这时库中的文件和本地文件又变为一致, 文件为`Unmodify`状态. 执行`git reset HEAD filename`取消暂存, 文件状态为`Modified`

## 4. Git的安装与使用

### 4.1 Git的安装

进入git的官网下载页面https://git-scm.com/downloads，下载对应操作系统的git的安装包，进行安装操作。

![image-20201120205051164](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120205051164.png)

如本次选择的是windows版本的安装包，安装完成后，在windows的右键菜单中，出现两个菜单项：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120205244591.png" alt="image-20201120205244591" style="zoom: 67%;" />

- Git GUI: 以图形界面的形式进行git的日常操作。
- Git Bash: 以命令行的方式进行git的日常操作，这也是推荐的一种操作。

点击Git Bash Here菜单项，即可打开Git的命令行工具，输入git version指令，即可看到当前安装的Git的版本：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120210000190.png" alt="image-20201120210000190" style="zoom:80%;" />

## 4.2 Git的使用

本小节以Idea和Gitee为例子，演示 两种操作：

1. 从远程仓库Gitee克隆Java项目代码到本地仓库，修改代码后上传到远程仓库。

2. 建立本地仓库，基于Idea创建Java项目，然后上传到Gitee远程仓库。

以上远程仓库的操作，需要先在https://gitee.com/上注册一个账户。

还需要配置本地git的Email相关信息：

```
$ git config --global user.name "你的名字或昵称"
$ git config --global user.email "你的邮箱"
```



### 4.2.1 远程仓库克隆项目代码到本地仓库

（1）找到一个远程仓库的地址，如https://gitee.com/jayliu007/java253.git，找一个合适的文件夹，如：

G:\ideaProjects，在文件夹空白处右键点击“Git Bash Here”，即可打开Git命令行窗口，并自动切换到该目录：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120221901621.png" alt="image-20201120221901621" style="zoom:80%;" />

（2）使用以下git指令，从远程仓库克隆项目到本地：

```
git clone https://gitee.com/jayliu007/java253.git
```

![image-20201120222014548](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120222014548.png)

克隆成功后，打开从远程仓库下载的文件夹，就可以看到完整的克隆到本地的项目：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120222129242.png" alt="image-20201120222129242" style="zoom:80%;" />

（3）使用idea打开克隆成功的项目，本次以idea2019.3版本为例进行演示：

在idea欢迎界面选择“open”,然后定位到刚下载成功的项目的根目录，点击“OK”

![image-20201120222505565](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120222505565.png)

即可打开从远程仓库克隆到本地的项目：

![image-20201120222643265](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120222643265.png)



### 4.2.2 上传本地仓库项目到远程仓库

1.创建远程仓库

点击顶部菜单栏上的“+”符号，在弹出的菜单项中，点击“新建仓库”：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120212305561.png" alt="image-20201120212305561" style="zoom:67%;" />

在弹出的新建窗口中，填入一下信息后，点击“创建”按钮，即可创建一个远程仓库。处于演示的目的，本小节选择创建一个公开的远程仓库 ，访问时，无需输入账号与密码。

![image-20201120212719812](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120212719812.png)

创建成功后，我们会看到远程仓库中包含一下文件：

![image-20201120212846743](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120212846743.png)

- .gitignore文件，作用是，当本地项目提交到远程仓库时，可以自动忽略一些不需要提交到远程仓库的文件，如java类编译后的字节码文件、开发工具自定义的配置文件等。
- LICENSE 开源许可协议文件
- README.MD相关文件，作用是对本项目的一个说明描述。

点击“克隆/下载按钮”，可以看到远程仓库的地址：https://gitee.com/jayliu007/java253.git

2.创建本地仓库

（1）使用Idea创建一个java项目：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120214309959.png" alt="image-20201120214309959" style="zoom:80%;" />



（2）在该目录的空白处，右键点击，选择“Git Bash Here”，即可打开Git命令行窗口，并自动切换到该目录：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120214504330.png" alt="image-20201120214504330" style="zoom:80%;" />



（3）使用以下指令，初始化本地仓库，并关联到对应的远程仓库：

```
git init 
git remote add origin https://gitee.com/jayliu007/java253.git
```

​	这样就完成了版本的一次初始化：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120215109771.png" alt="image-20201120215109771" style="zoom:80%;" />

在当前项目的根目录，就会出现.git这个隐藏的文件夹(需要先进行显示隐藏的项目的操作)：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120215333507.png" alt="image-20201120215333507" style="zoom:80%;" />



（4）从远程仓库拉取最新的文件：

```
git pull origin master
```

拉取成功后，就会在项目的根目录看到远程仓库中的那几个文件，出现在项目的根目录中：

![image-20201120215514339](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120215514339.png)

![image-20201120215537674](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120215537674.png)



（5）提交本地项目代码到远程仓库

```
git add .
git commit -m "第一次提交,初始化项目"
git push origin master
```

然后如果需要账号密码的话就输入账号密码，这样就完成了一次从本地到远程的提交。此时，在个人面板、仓库主页查看到当前的提交记录。

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120215646526.png" alt="image-20201120215646526" style="zoom:80%;" />

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120215711669.png" alt="image-20201120215711669" style="zoom:80%;" />

PS: 讲解为什么本地的out文件夹没有被上传到远程仓库。。。。

### 4.2.3 添加/修改本地代码后，上传到远程仓库

本小节以添加一个Test.java文件为例进行讲解。

（1）添加新的类文件Test.java到项目后，该文件是一个新的文件，其状态对应着前面小节讲到的git的文件状态-**Untracked**，但是该文件被idea检测到，会提示是否把该文件加入到git： 

![image-20201120223159068](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120223159068.png)

**Untracked**状态的文件，在idea中，呈红色字体状态显示。

（2）点击“Add”后，该文件状态变为绿色，提示用户该文件加入到git成功，此时该文件状态为**Staged**暂存状态

![image-20201120223259029](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120223259029.png)

（3）执行git commit指令，则可以将其修改同步到本地仓库中，或者直接使用idea的UI进行操作：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120224318014.png" alt="image-20201120224318014" style="zoom:80%;" />

点击"Commit File"后，即可打开提交到本地的对话窗口，该操作可以直接使用ctrl+k快捷键：

<img src="https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120224502424.png" alt="image-20201120224502424" style="zoom: 67%;" />

输入注释信息后(每次提交一定要写注释)，点击Commit即可提交到本地仓库，此时，文件为`Unmodify`状态。

（4）修改文件后，一般文件状态会变为**Modified**状态，这个文件也有两个去处, 通过`git add`可进入暂存`staged`状态, 使用`git checkout` 则丢弃修改, 返回到`unmodify`状态, 这个`git checkout`即从库中取出文件, 覆盖当前修改。

但是在idea中，进行操作，非常简单，如果我们希望修改生效，则只需再次commit到本地即可。如果不想被修改，直接选择右键菜单中的"Rollback"， 或者使用快捷键ctrl+alt_+z进行操作：

![image-20201120225132268](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120225132268.png)

（5）提交代码到远程仓库

代码提交到本地仓库后，就可以提交到远程仓库了。

注意：提交代码到远程仓库前，一定要先从远程仓库拉取最新的代码，重要的事情说三遍！！！

右键选择Push，或者直接使用快捷键ctrl+shit+k, 打开push到远程的对话框：

![image-20201120225504717](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120225504717.png)

在该对话框中，可以看到有两次本地提交，对应着两个本地版本，可以选择提交哪个版本，如我们选择第二次提交的，点击“Push”。

![image-20201120225722268](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120225722268.png)

![image-20201120225735007](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120225735007.png)

push成功后，就可以在远程仓库看到提交的最新的代码了：

![image-20201120225820753](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120225820753.png)

![image-20201120225850708](https://jayoss.oss-cn-beijing.aliyuncs.com/images/image-20201120225850708.png)

上图“+”符号表示的是最新添加的，它只是一种提示符号，真实的源代码文件中，是不会存在的。

PS：后续课堂上演示提交产生冲突该如何解决。