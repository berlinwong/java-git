package debug;

public class OrderingSet {
    //订餐人
    private String userName;
    //菜品名
    private String dishName;
    //菜品份数
    private int dishNum;
    //送餐实践-时
    private String time;
    //送餐地址
    private String address;
    //送餐状态：0-已下单，未签收;1-已签收
    private int state;
    //订单总价
    private double sumPrice;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public int getDishNum() {
        return dishNum;
    }

    public void setDishNum(int dishNum) {
        this.dishNum = dishNum;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public double getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(double sumPrice) {
        this.sumPrice = sumPrice;
    }

    @Override
    public String toString() {
        return "OrderingSet{" +
                "userName='" + userName + '\'' +
                ", dishName='" + dishName + '\'' +
                ", dishNum=" + dishNum +
                ", time='" + time + '\'' +
                ", address='" + address + '\'' +
                ", state=" + state +
                ", sumPrice=" + sumPrice +
                '}';
    }
}
