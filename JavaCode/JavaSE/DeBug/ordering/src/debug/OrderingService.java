package debug;

import java.util.Scanner;

public class OrderingService {
    //保存菜单数据
    private MenuItem[] menuItems = new MenuItem[4];
    //保存点餐订单数据
    private OrderingSet[] orderingSets = new OrderingSet[5];
    //获取用户输入对象
    private Scanner scanner = new Scanner(System.in);

    public void initial() {
        //初始化菜单数据
        MenuItem chicken = new MenuItem("黄焖鸡", 12);
        MenuItem fish = new MenuItem("鱼香肉丝", 20);
        MenuItem toufu = new MenuItem("麻婆豆腐", 10);
        MenuItem potato = new MenuItem("清炒土豆丝", 8);
        menuItems[0] = chicken;
        menuItems[1] = fish;
        menuItems[2] = toufu;
        menuItems[3] = potato;
        //初始化订单数据
        OrderingSet order1 = new OrderingSet();
        order1.setUserName("托尼");
        order1.setDishName("黄焖鸡");
        order1.setDishNum(2);
        order1.setTime("3");
        order1.setAddress("苏园路");
        order1.setState(1);
        order1.setSumPrice(50);

        OrderingSet order2 = new OrderingSet();
        order2.setUserName("李莉莉");
        order2.setDishName("鱼香肉丝");
        order2.setDishNum(2);
        order2.setTime("3");
        order2.setAddress("苏园路");
        order2.setState(0);
        order2.setSumPrice(45);
        orderingSets[0] = order1;
        orderingSets[1] = order2;
    }

    /**
     * 系统菜单
     */
    public void startMenu() {
        System.out.println("欢迎使用【吃了吗】订单系统");
        boolean goON = true;
        do {
            System.out.println("********************");
            System.out.println("1.我要订餐");
            System.out.println("2.查看订餐");
            System.out.println("3.签收订单");
            System.out.println("4.删除订单");
            System.out.println("5.我要点赞");
            System.out.println("6.退出系统");
            System.out.println("********************");
            System.out.print("请选择:");
            if (scanner.hasNextInt()) {
                int option = scanner.nextInt();
                //判断选项
                switch (option) {
                    case 1:
                        add();
                        break;
                    case 2:
                        //实现查看餐袋
                        display();
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        goON = false;
                        break;
                }
            } else {
                System.out.println("输入有误，请重输");
                scanner.next();
            }
        } while (goON);

        System.out.println("已退出系统");
    }

    /**
     * 显示订单数据
     */
    private void display() {
        System.out.println("查看订餐：\n*********");
        System.out.printf("%2s\t%8s\t%10s\t%4s\t%10s\t%4s\t%4s\n",
                "序号", "订餐人", "餐品信息", "送餐时间", "送餐地址", "总金额", "订单状态");
        for (int i = 0; i < this.orderingSets.length; i++) {
            if (orderingSets[i] == null) {
                break;
            }
            //打印数据
            String state = orderingSets[i].getState() == 1 ? "已完成" : "已预定";
            System.out.printf("%2s\t%8s\t%10s\t%4s\t%10s\t%4s\t%4s\n",
                    (i + 1), orderingSets[i].getUserName(), orderingSets[i].getUserName() + " " + orderingSets[i].getDishName() + "份",
                    orderingSets[i].getTime() + "时",
                    orderingSets[i].getAddress(), orderingSets[i].getSumPrice() + "元",
                    state);
        }
    }

    /**
     * 新增订单
     */
    private void add() {
        //判断有没有新增位置。没有就给出提示，有就新增
        int index = -1;//保存要插入的新的订单的位置
        for (int i = 0; i < this.orderingSets.length; i++) {
            if (this.orderingSets[i] == null) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            System.out.println("已打烊");
        } else {
            System.out.println("我要订餐：\n*********");
            OrderingSet orderingSet = new OrderingSet();
            orderingSets[index] = orderingSet;
            Scanner sc = new Scanner(System.in);
            System.out.print("请输入订餐人姓名：");
            orderingSets[index].setUserName(sc.next());
            //显示菜单
            displayMenu();
            //开始点菜
            System.out.print("请选择你要点的菜品编号：");
            int i = sc.nextInt();
            orderingSets[index].setDishName(menuItems[i - 1].getDishName());
            System.out.print("请选择你需要的份数：");
            orderingSets[index].setDishNum(sc.nextInt());
            System.out.print("请输入送餐时间（10点到20点整点送餐）：");
            orderingSets[index].setTime(sc.next());
            System.out.print("请输入送餐地址：");
            orderingSets[index].setAddress(sc.next());
            orderingSets[index].setSumPrice(menuItems[i - 1].getPrice() * orderingSets[index].getDishNum() + 5);
            System.out.println("订餐成功");
            System.out.printf("您订的是：%5s\t%d份\n", orderingSets[index].getDishName(), orderingSets[index].getDishNum());
            System.out.printf("送餐时间：%s时\n", orderingSets[index].getTime());
            System.out.printf("餐费：%f元，送餐费5.0元，总计：%f元", menuItems[i - 1].getPrice(), orderingSets[index].getSumPrice());
        }
    }

    private void displayMenu() {
        System.out.printf("%2s\t%8s\t%8s\t%4s\n", "序号", "菜名", "单价", "点赞数");
        int i = 1;
        for (MenuItem menuItem : menuItems) {
            System.out.printf("%2d\t%8s\t%8s\t%4d\n", i++, menuItem.getDishName(), menuItem.getPrice() + "元",
                    menuItem.getPrizeNum());

        }

    }
}
