/**
 * ClassName:DoIt
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-12 9:52
 * @author: Berlin 1412124522@qq.com
 */
public class DoIt {
    public static void main(String[] args) {
        Thread myThreadDemo = new MyThread();
        Thread myThreadDemoII = new MyThread();
        Runnable myRunnable = new MyRunnable();

        Thread thread1 = new Thread(myRunnable);
        Thread thread2 = new Thread(myRunnable);
//        thread1.setPriority(10);
//        myThreadDemo.start();
//        myThreadDemoII.start();
        thread1.start();
//        thread2.start();


    }
}
