/**
 * ClassName:MyRunnable
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-12 13:53
 * @author: Berlin 1412124522@qq.com
 */
public class MyRunnable implements Runnable {

    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        for (int i = 10; i>0; i--) {
//            System.out.println(Thread.currentThread().getName() + "好好学习，天天向上！");
            try {
                Thread.sleep(1800000);

                System.out.println(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Boom！");

    }
}
