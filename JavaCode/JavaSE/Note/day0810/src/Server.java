import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * ClassName:SocketDemo
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-10 21:53
 * @author: Berlin 1412124522@qq.com
 */
public class Server {

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(409);
            System.out.println("服务器【" + serverSocket.getInetAddress() + "】 启动成功 ... ");
            while (true) {
                Socket accept = serverSocket.accept();
                System.out.println("与客户端【" + accept.getRemoteSocketAddress() + "】 连接成功 ... ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
