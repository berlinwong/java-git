/**
 * ClassName:TestWriter
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-10 11:18
 * @author: Berlin 1412124522@qq.com
 */


import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * @author jay
 * @date 2021/8/10
 * @desc 测试文件写入操作
 */
public class TestWriteri {
    public static void main(String[] args) {
        // 测试写入到一个不存在的文件里
        String path = "d:\\test3.txt";
        Writer writer = null;
        try {
            writer = new FileWriter(path,false);
            writer.write("我是中国人");
            writer.flush();
            writer = new FileWriter(path,false);
            writer.write("abcd");
            writer.flush();
            writer = new FileWriter(path,true);
            writer.write("abcd");
            writer.flush();

            // 可以不调用，调用的目的，是为了提高写的效率


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
