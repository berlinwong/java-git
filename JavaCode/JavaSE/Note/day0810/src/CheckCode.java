import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static java.lang.Math.pow;

/**
 * ClassName:CheckCode
 * Package:PACKAGE_NAME
 * Description: 实现短信模板，【{from}】验证码{code}，有效期{minute}分钟。
 *
 * @date: 21-8-10 9:50
 * @author: Berlin 1412124522@qq.com
 */
public class CheckCode {

    // 定义初始文件路径
    static String basePath = "D:\\Berlin\\Desktop\\java-git\\JavaCode\\JavaSE\\Note\\day0810\\dic\\scode.txt";

    /**
     * Description: 测试主方法
     *
     * @author: Berlin
     * @date: 21-8-10 16:17
     * @param:
     * @return:
     */
    public static void main(String[] args) {
//        for (int i = 0; i < 100; i++) {
//            System.out.println(rndInt(4));
//        }

//        System.out.println(Arrays.toString(info));
//        cover(info);
        for (int i = 0; i < 520; i++) {
            String[] info = prepare("中国联通", 4, "5");
            replace(info);
        }


    }

    /**
     * Description: 准备参数，写入文件指定位置
     *
     * @author: Berlin
     * @date: 21-8-10 10:09
     * @param: fromWho
     * @param: length
     * @param: timeLimit
     * @return: strings
     */
    public static String[] prepare(String fromWho, int length, String timeLimit) {
        final String[] strings = new String[3];
        strings[0] = fromWho;
        strings[1] = String.valueOf(rndInt(length));
        strings[2] = timeLimit;
        return strings;
    }

    /**
     * Description: 生成length长度的随机数
     *
     * @author: Berlin
     * @date: 21-8-10 10:08
     * @param: length 需要验证码的长度
     * @return: resCode
     */
    public static int rndInt(int length) {
        int resCode = 0;
        resCode = (int) (Math.random() * (pow(10, length) - pow(10, length - 1) + 1) + pow(10, length - 1));
        return resCode;
    }

    /**
     * Description: 载入短信模板，遇到{} 进行替换
     *
     * @author: Berlin
     * @date: 21-8-10 10:12
     * @param:
     * @return:
     */
    public static void replace(String[] info) {
        FileInputStream fileInputStream = null;
        InputStreamReader streamReader = null;
        BufferedWriter bw = null;
        BufferedReader br = null;
        try {
            fileInputStream = new FileInputStream(basePath);
            streamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
            br = new BufferedReader(streamReader);
            String datas = br.readLine();
            while (br.readLine() != null) {
                datas = br.readLine();
            }
            String model = datas;
//            System.out.println(datas);
//            System.out.println(model);
            model = model.replace("{from}", info[0]);
            model = model.replace("{code}", info[1]);
            model = model.replace("{minute}", info[2]);
            System.out.println("添加成功：" + model);
            FileOutputStream fileOutputStream =
                    new FileOutputStream(
                            "D:\\Berlin\\Desktop\\java-git\\JavaCode\\JavaSE\\Note\\day0810\\dic\\scodeRes.txt",
                            true
                    );
            Writer writer = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
            bw = new BufferedWriter(writer);
            bw.write(model);
            bw.newLine();
            bw.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                if (streamReader != null) {
                    streamReader.close();
                }
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * Description: 直接覆盖写入型
     *
     * @author: Berlin
     * @date: 21-8-10 16:16
     * @param:
     * @return:
     */
    public static void cover(String[] info) {
        FileOutputStream fileOutputStream = null;
        BufferedWriter bw = null;
        Writer write = null;
        try {
            fileOutputStream = new FileOutputStream(basePath, true);
            write = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
            bw = new BufferedWriter(write);
            String content = "【%s】验证码 %s ，有效期 %s 分钟。".formatted(info[0], info[1], info[2]);
            bw.write(content);
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (write != null) {
                    write.close();
                }
                if (bw != null) {
                    bw.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
