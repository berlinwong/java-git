import java.io.IOException;
import java.net.Socket;
import java.util.Locale;
import java.util.Scanner;

/**
 * ClassName:client
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-10 22:00
 * @author: Berlin 1412124522@qq.com
 */
public class Client {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Socket sock = null;
        try {
            sock = new Socket("192.168.168.3", 409, null, 4091);
            System.out.println("与服务器【" + sock.getRemoteSocketAddress() + "】 连接成功 ... ");
            String answer = "";
            while (true) {
                System.out.println("是否与服务器断开连接？[Y/N]");
                answer = scanner.next();
                if ("y".equals(answer.toLowerCase())) {
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (sock!=null) {
                    sock.close();
                }
                scanner.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
