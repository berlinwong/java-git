package com.berlin.orderapp;

import java.util.Scanner;

/**
 * @author Berlin
 * @date 21-8-2
 * @desc 点餐系统服务类
 */

public class OrderingService {
    //保存菜单数据
    private MenuItem[] menuItems = new MenuItem[4];
    //保存点餐订单数据
    private OrderingSet[] orderingSets = new OrderingSet[5];
    private Scanner scanner = new Scanner(System.in);

    public void initial() {
        for (int i = 0; i < menuItems.length; i++) {
            menuItems[i] = new MenuItem();
        }
        menuItems[0].setDishName("黄焖鸡");
        menuItems[0].setPrice(25.0);
        menuItems[0].setPrizeNum(0);
        menuItems[1].setDishName("鱼香肉丝");
        menuItems[1].setPrice(20.0);
        menuItems[1].setPrizeNum(0);
        menuItems[2].setDishName("麻婆豆腐");
        menuItems[2].setPrice(10.0);
        menuItems[2].setPrizeNum(0);
        menuItems[3].setDishName("清炒土豆丝");
        menuItems[3].setPrice(8.0);
        menuItems[3].setPrizeNum(0);


        OrderingSet order1 = new OrderingSet();
        order1.setUserName("托尼");
        order1.setDishName("黄焖鸡");
        order1.setAddress("苏元禄五号");
        order1.setDishNum(2);
        order1.setSumPrice(50);
        order1.setTime("3");
        order1.setState(0);

        OrderingSet order2 = new OrderingSet();
        order2.setUserName("胡阿右");
        order2.setDishName("鱼香肉丝");
        order2.setAddress("苏元禄五号");
        order2.setDishNum(2);
        order2.setSumPrice(45);
        order2.setTime("3");
        order2.setState(1);

        orderingSets[0] = order1;
        orderingSets[1] = order2;

    }

    //开始菜单
    public void startMenu() {
        System.out.println("欢迎使用【吃了吗订餐系统】");
        boolean goOn = true;
        do {
            System.out.println("*************************");
            System.out.println(" 1、我要订餐 2、查看餐袋 3、签收订单 4、删除订单 5、我要点赞 6、退出系统");
            System.out.println("请选择：");
            if (scanner.hasNextInt()) {
                int option = scanner.nextInt();
                switch (option) {
                    case 1:
                        // 我要订餐
                        orderingNow();
                        break;
                    case 2:
                        // 查看餐袋
                        showOrderingSets();
                        break;
                    case 3:
                        sign();
                        break;
                    case 4:
                        delete();
                        break;
                    case 5:
                        praise();
                        break;
                    case 6:
                        goOn = false;
                        break;
                    default:
                        System.out.println("请输入正确选项！");
                        break;
                }
            } else {
                System.out.println("输入有误！请重试。");
                scanner.next();
            }
        } while (goOn);
        System.out.println("拜拜咯，您嘞！");


    }

    // 展示菜单 -> 给点单调用
    public void showMenu() {
        int count = 0;
        System.out.printf("%2s\t%6s\t   %6s\t%2s\n",
                "序号",
                "菜肴",
                "单价",
                "好评"

        );
        System.out.println("——————————————————————————————");
        for (MenuItem menuItem : menuItems) {

            System.out.printf("%2d\t%6s\t%6s\t%2s\n",
                    count,
                    menuItem.getDishName(),
                    menuItem.getPrice(),
                    menuItem.getPrizeNum()

            );
            count += 1;

        }
    }

    // 计算总价格 -> 给点单调用
    public double getPrice(int orderNum) {
        return menuItems[orderNum].getPrice();

    }

    // 点单
    private void orderingNow() {
        int index = -1;
        for (int i = 0; i < orderingSets.length; i++) {
            if (orderingSets[i] == null) {
                index = i;
                break;
            }

        }
        System.out.println("欢迎光临本小店请问如何称呼您：");
        String userName = scanner.next();
        System.out.println("您点个什么呢：");
        showMenu();
        int orderNum = scanner.nextInt();
        double priceThis = getPrice(orderNum);
        System.out.println("您点几份呢：");
        int muchNum = scanner.nextInt();
        double sumPrice = (muchNum * priceThis) + 5;
        System.out.println("您的地址：");
        String address = scanner.next();
        System.out.println("您预想几点送达：");
        String times = scanner.next();
        OrderingSet order3 = new OrderingSet();
        order3.setUserName(userName);
        order3.setDishName(menuItems[orderNum].getDishName());
        order3.setAddress(address);
        order3.setDishNum(muchNum);
        order3.setSumPrice(sumPrice);
        order3.setTime(times);
        order3.setState(0);
        orderingSets[index] = order3;


    }


    // 查看订单信息
    private void showOrderingSets() {
        System.out.printf("%4s \t%4s \t%8s \t  %8s\t%6s\t%8s\t%8s\n",
                "序号", "昵称", "餐品状态", "参品信息", "地址", "总价", "送达时间");
        int count = 0;
        for (OrderingSet orderingSet : orderingSets) {
            if (orderingSet != null) {
                String state = "";
                if (orderingSet.getState() == 1) {
                    state = "已完成";
                } else {
                    state = "未完成";
                }
                System.out.printf("%4d\t%4s\t%8s\t%8s%2s份\t%8s\t  %.1f\t%8s点\n",
                        count,
                        orderingSet.getUserName(),
                        state,
                        orderingSet.getDishName(),
                        orderingSet.getDishNum(),
                        orderingSet.getAddress(),
                        orderingSet.getSumPrice(),
                        orderingSet.getTime());
                count += 1;
            }
        }
        System.out.println();
    }

    // 订单签收功能
    private void sign() {
        System.out.println("*** 订单签收 ***");
        System.out.println("请选择您需要签收的订单号：");
        int signID = scanner.nextInt();
        boolean flag = true;
        do {
            if (signID >= orderingSets.length || orderingSets[signID] == null || orderingSets[signID].getUserName() == null) {
                System.out.println("您选择的订单不存在!重新输入：");
                signID = scanner.nextInt();
            } else {
                if (orderingSets[signID].getState() == 1) {
                    System.out.println("订单已经签收过了！为您返回主菜单！");
                    System.out.println();
                    flag = false;
                } else {
                    orderingSets[signID].setState(1);
                    System.out.println("订单签收成功");
                    System.out.println();
                    break;
                }

            }

        } while (flag);
    }

    // 订单删除功能
    private void delete() {
        System.out.println("*** 订单删除 ***");
        System.out.println("请选择您需要删除的订单号：");
        int deleteID = scanner.nextInt();
        boolean flag = true;
        do {
            if (deleteID >= orderingSets.length || orderingSets[deleteID] == null || orderingSets[deleteID].getUserName() == null) {
                System.out.println("您选择的订单不存在!重新输入：");
                deleteID = scanner.nextInt();
            } else {
                if (orderingSets[deleteID].getState() == 1) {
                    orderingSets[deleteID] = null;
                    flag = false;
                    System.out.println("删除成功！感谢下次光临！");
                } else if (orderingSets[deleteID].getState() == 0) {
                    System.out.println("当前订单未完成，请先签收后删除！已返回主菜单！");
                    flag = false;
                }
            }
        } while (flag);
    }

    // 订单点赞功能 调用showMenu();
    private void praise() {
        System.out.println("*** 菜肴点赞 ***");
        showMenu();
        System.out.println("请选择您需要点赞的菜品编号：");
        int dishID = scanner.nextInt();
        boolean flag = true;
        do {
            if (dishID < menuItems.length && menuItems[dishID] != null) {
                menuItems[dishID].setPrizeNum(menuItems[dishID].getPrizeNum() + 1);
                System.out.println("点赞成功！曾有" + (menuItems[dishID].getPrizeNum() - 1) + "个顾客也点赞过该菜品！");
                flag = false;
            } else {
                System.out.println("当前序号不在菜单中，如果您希望更多菜品被推出，请留下您的赞！");
                dishID = scanner.nextInt();

            }
        } while (flag);


    }


}




