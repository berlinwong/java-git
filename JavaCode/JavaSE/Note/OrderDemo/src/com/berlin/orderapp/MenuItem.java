package com.berlin.orderapp;

/**
 * @author Berlin
 * @date 21-8-2
 * @desc 菜单项 - 菜单信息
 */
public class MenuItem {
    private String dishName;
    private double price;
    private int prizeNum;

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public double getPrice() {
        return price;
    }


    public void setPrice(double price) {
        this.price = price;
    }

    public int getPrizeNum() {
        return prizeNum;
    }

    public void setPrizeNum(int prizeNum) {
        this.prizeNum = prizeNum;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "dishName='" + dishName + '\'' +
                ", price=" + price +
                ", prizeNum=" + prizeNum +
                '}';
    }
}
