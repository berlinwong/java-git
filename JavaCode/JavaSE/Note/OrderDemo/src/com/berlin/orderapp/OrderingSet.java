package com.berlin.orderapp;

/**
 * @author Berlin
 * @date 21-8-2
 * @desc 订单业务实体类
 */
public class OrderingSet {
    private String userName;
    private String dishName;
    private int dishNum;
    private String time;
    private String address;
    // 0：下单未签收 1：签收
    private int state;
    private double sumPrice;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public int getDishNum() {
        return dishNum;
    }

    public void setDishNum(int dishNum) {
        this.dishNum = dishNum;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public double getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(double sumPrice) {
        this.sumPrice = sumPrice;
    }


    @Override
    public String toString() {
        return "OrderingSet{" +
                "userName='" + userName + '\'' +
                ", dishName='" + dishName + '\'' +
                ", dishNum=" + dishNum +
                ", time='" + time + '\'' +
                ", address='" + address + '\'' +
                ", state=" + state +
                ", sumPrice=" + sumPrice +
                '}';
    }
}
