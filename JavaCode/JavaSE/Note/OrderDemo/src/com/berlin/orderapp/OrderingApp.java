package com.berlin.orderapp;

/**
 * @author Berlin
 * @date 21-8-2
 * @desc 入口程序
 */
public class OrderingApp {
    public static void main(String[] args) {
        OrderingService orderingService = new OrderingService();
        orderingService.initial();
        orderingService.startMenu();
    }
}
