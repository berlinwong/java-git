import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * ClassName: DBHelper
 * Package: PACKAGE_NAME
 * Description: 连接Oracle的工具人
 *
 * @date: 2021/8/7 3:59 下午
 * @author: 1412124522@qq.com
 */
public class DBHelper {
    private static String userName;
    private static String password;
    private static String urlStr;


    /**
     * @Title: 加载驱动
     * @Description //TODO
     * @Date 4:11 下午 2021/8/7
     * @param
     * @return
     * @throws
     */
    static{
        // 初始化驱动载入账户信息
        init();

    }

    /**
     * @Title: 初始化
     * @Description //TODO
     * @Date 4:33 下午 2021/8/7
     * @param
     * @return
     * @throws
     */

    public static void init() {
        Properties prop = new Properties();
        InputStream is = DBHelper.class.getClassLoader().getResourceAsStream("db.properties");
        try {
            prop.load(is); // 载入配置文件
            userName = prop.getProperty("userName"); //从配置文件啊获取用户名、密码、连接地址以及包名
            password = prop.getProperty("password");
            urlStr = prop.getProperty("urlStr");
            String classNameStr = prop.getProperty("classNameStr");
            Class.forName(classNameStr);
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Title: 创建连接对象
     * @Description //TODO
     * @Date 4:10 下午 2021/8/7
     * @param
     * @return Connection
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(urlStr, userName, password);
        return connection;
        // TODO 是否可以直接返回 而不是创建对象
    }

    /**
     * @Title: 关闭数据库连接
     * @Description //TODO
     * @Date 4:13 下午 2021/8/7
     * @param
     * @return
     * @throws SQLException

     */

    public static void close(Connection connection) throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    public static void close(ResultSet rs, Statement st, Connection connection) throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (connection != null) {
            connection.close();
        }
        if (st != null) {
            st.close();
        }
    }

}
