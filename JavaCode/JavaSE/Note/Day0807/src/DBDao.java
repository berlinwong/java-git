import java.sql.SQLException;

/**
 * ClassName: DBDao
 * Package: PACKAGE_NAME
 * Description:
 *
 * @date: 2021/8/7 6:02 下午
 * @author: 1412124522@qq.com
 */
public interface DBDao {
    // 查看所有员工
    void selectData() throws SQLException;

    /**
     * @Title: 删除员工
     * @Description //TODO
     * @Date 6:51 下午 2021/8/7
     * @param EMPNO 员工号
     * @return 0 / 1
     * @throws SQLException
     */
    int deleteData(int EMPNO) throws SQLException;


    void updateData(String numNum);

    void updateData(int rowID);

    void insertData();


}
