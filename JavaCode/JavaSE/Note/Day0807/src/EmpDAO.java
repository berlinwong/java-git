import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * ClassName: EmpDAO
 * Package: PACKAGE_NAME
 * Description:
 *
 * @date: 2021/8/7 6:13 下午
 * @author: 1412124522@qq.com
*/
public class EmpDAO implements DBDao{
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;

    /**
     * @Title: 查看员工表信息
     * @Description //TODO
     * @Date 6:34 下午 2021/8/7
     * @param
     * @return
     * @throws
     */
    @Override
    public void selectData() throws SQLException {
        conn = DBHelper.getConnection();
        stmt = conn.createStatement();
        rs = stmt.executeQuery("select * from emp");
        System.out.print("员工编号\t\t姓名\t\t部门编号\n");
        System.out.println("---------------------------");
        while (rs.next()) {
            System.out.printf("  %4d\t%8s\t  %2d\n",rs.getInt(1),rs.getString(2),rs.getInt("deptno"));
        }
        System.out.println("---------  END  ---------");
        DBHelper.close(rs,stmt,conn);
    }

    @Override
    public int deleteData(int EMPNO) throws SQLException {
        conn = DBHelper.getConnection();
        stmt = conn.createStatement();
        String sql = "delete from emp where EMPNO = " + EMPNO;
        int res = stmt.executeUpdate(sql);
        DBHelper.close(rs,stmt,conn);
        return res;
    }

    @Override
    public void updateData(String numNum) {

    }

    @Override
    public void updateData(int rowID) {

    }

    @Override
    public void insertData() {

    }
}
