import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * ClassName: JbdcTs
 * Package: PACKAGE_NAME
 * Description:
 *
 * @date: 2021/8/6 2:45 下午
 * @author: 1412124522@qq.com
 */


// jdbc:oracle:thin:@localhost:1521:helowin
public class JbdcTs {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        connJBDC("SCOTT","tiger","helowin");
    }

/**
 * @Title: 连接数据库
 * @Description //TODO 
 * @Date 4:00 下午 2021/8/6
 * @param 
 * @return 
 * @throws
 */

    public static void connJBDC( String username, String password,String className){
        Connection conn = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@localhost:1521:" + className;
//            String username = "SCOTT";
//            String password = "tiger";
            conn = DriverManager.getConnection(url, username, password);
            System.out.println("连接成功！");
            System.out.println(conn);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
