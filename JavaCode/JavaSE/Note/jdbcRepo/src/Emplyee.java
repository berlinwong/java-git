import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * ClassName: Emplyee
 * Package: PACKAGE_NAME
 * Description:
 *
 * @date: 2021/8/7 8:54 上午
 * @author: 1412124522@qq.com
 */
public class Emplyee {
    public static void main(String[] args) throws Exception {
        updateEmplyee("SCOTT", 400);
    }

    public static int updateEmplyee(String empName, double salary) throws Exception {
        Connection connection = null;
        Statement statement = null;
        try {
            int result = 0;
            // 1.加载驱动
            Class.forName("oracle.jdbc.driver.OracleDriver");
            // 2.创建连接对象
            String url = "jdbc:oracle:thin:@localhost:1521:" +
                    "helowin";
            String username = "scott";
            String password = "tiger";
            connection = DriverManager.getConnection(url, username, password);
            // 3.进行crud操作
            // 3.1 创建Statement对象
            statement = connection.createStatement();
            // 3.2 准备sql语句
//            String sql = "update emp set sal = sal+" + salary +
//                    "  where ename = '" + empName + "'";
            String sql = "delete from emp where EMPNO = '7934'";

            // 3.3执行sql语句
            result = statement.executeUpdate(sql);

            return result;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            // 3.关闭连接
            // 先释放statement资源
            if (statement != null) {
                statement.close();
                System.out.println("成功！");
            }
            if (connection != null) {
                connection.close();
            }
        }
        return 0;
    }

}
