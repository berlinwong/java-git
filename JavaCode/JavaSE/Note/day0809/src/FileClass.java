import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Scanner;
import java.util.zip.InflaterInputStream;

/**
 * ClassName:FileClass
 * Package:PACKAGE_NAME
 * Description: 熟悉IO的File类
 *
 * @date: 21-8-9 23:02
 * @author: Berlin 1412124522@qq.com
 */
public class FileClass {
    //    static String basePath = "D:\\Berlin\\Desktop\\java-git\\JavaCode\\JavaSE\\Note\\day0809\\catalogue\\";
    static String basePath;

    /**
    * Description: 初始化配置文件
    * @author: Berlin
    * @date: 21-8-10 9:45
    * @param:
    * @return:
    */
    static {
        init();
    }

    /**
    * Description: 加载配置文件
    * @author: Berlin
    * @date: 21-8-10 9:42
    * @param:
    * @return:
    */
    public static void init() {
        Properties prop = new Properties();
        InputStream is = FileClass.class.getClassLoader().getResourceAsStream("path.properties");
        try {
            prop.load(is);
            basePath = prop.getProperty("basePath");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
    * Description: 主方法
    * @author: Berlin
    * @date: 21-8-10 9:43
    * @param:
    * @return:
    */
    public static void main(String[] args) {
//        baseFileInfo(basePath,"testFile.txt"); // 显示文件基本信息
//        createFile(basePath, "我爱你IO.txt"); // 创建文件
//        deleteFile(basePath, "我爱你IO.txt"); // 删除文件
//        fileInputStreamB(basePath, "testFile.txt"); // 有缓冲区文本打印
//        bufferFileInputStream(basePath, "testFile.txt"); // 有缓冲区文本打印
//        FileReaderTs(basePath, "我爱你.txt"); //文件不存在异常测试
//        FileReaderTs(basePath, "我爱你IO.txt"); // 打印文本内容
          FileReaderTs(basePath, "蜀道难.txt"); // 打印文本内容
//        deleteFile(basePath, "我爱你.txt"); // 删除文件测试
//        bufferWriterFile(basePath, "蜀道难.txt"); // bufferedWriter测试
    }

    /**
     * Description: File 常用方法
     *
     * @author: Berlin
     * @date: 21-8-9 23:10
     * @param:
     * @return:
     */
    public static void baseFileInfo(String path, String fileName) {
//        String path = "D:\\Berlin\\Desktop\\java-git\\JavaCode\\JavaSE\\Note\\day0809\\catalogue\\";
//        String fileName = "testFile.txt";
        File file = new File(path + fileName);
        System.out.println("||- 文件是否存在: " + file.exists()); // 判断文件是否存在
        System.out.println("||- 是否为文件: " + file.isFile()); // 判断是否为文件
        System.out.println("||- 是否为目录: " + file.isDirectory()); // 判断是否为目录
        System.out.println("||- 路径: " + file.getPath()); // 获取路径
        System.out.println("||- 绝对路径: " + file.getAbsolutePath()); // 获取绝对路径
        System.out.println("||- 文件名: " + file.getName()); // 获取文件名
        System.out.println("||- 文件大小: " + file.length() + " Byte"); // 获取文件大小 long 单位字节 byte
    }

    /**
     * Description: 创建文件夹
     *
     * @author: Berlin
     * @date: 21-8-9 23:21
     * @param: path 获取文件所在路径
     * @param: fileName 需要创建文件的文件名
     * @return:
     */
    public static void createFile(String path, String fileName) {
        File file = new File(path + fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                System.out.println(file.getAbsolutePath());
            }
        } else {
            System.out.println("文件已存在...");
        }


    }

    /**
     * Description: 删除文件
     *
     * @author: Berlin
     * @date: 21-8-9 23:22
     * @param:
     * @return:
     */
    public static void deleteFile(String path, String fileName) {
        File file = new File(path + fileName);
        // 判断文件是否存在
        if (file.exists()) {
            file.delete();
            System.out.println("删除成功！");
        } else {
            System.out.println("ERROR! 文件不存在！");
        }

    }

    /**
     * Description: 无缓冲区文件流处理 InputStream
     *
     * @author: Berlin
     * @date: 21-8-9 23:32
     * @param: path
     * @param: fileName
     * @return:
     */
    public static void fileInputStreamB(String path, String fileName) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path + fileName);
            System.out.println("READ FROM: " + path + "\n" + "--> 击中: " + fileName + " ...");
            System.out.println("---------------------------------------------------------------------------");
            while (true) {
                int word = fis.read(); // 读取文件的ASCII值
                System.out.print("ASCII-Value: " + word);
                if (word == -1) { // 最后一个元素为-1，标志文件结束
                    break;
                }
                System.out.println(" \t->\tChar-Value: " + (char) word);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭文本流
            try {
                if (fis != null) {
                    fis.close();
                    System.out.println("\nfis is closed ...");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Description: 带有缓冲区的文件流处理 并且使用自动关闭资源写法
     *
     * @author: Berlin
     * @date: 21-8-10 0:02
     * @param: path
     * @param: fileName
     * @return:
     */
    public static void bufferFileInputStream(String path, String fileName) {
        try (FileInputStream fis = new FileInputStream(path + fileName)) {
            System.out.println("READ FROM: " + path + "\n" + "--> 击中: " + fileName + " ...");
            System.out.println("---------------------------------------------------------------------------");
            int length = fis.available();
            byte[] datas = new byte[length];
            fis.read(datas);
            for (int i = 0; i < length; i++) {
                System.out.print((char) datas[i] + "\t");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Description: 解决FileInputStream 乱码问题 使用FileReader解决
     *
     * @author: Berlin
     * @date: 21-8-10 0:35
     * @param:
     * @return:
     */
    public static void FileReaderTs(String path, String fileName) {
        BufferedReader bf = null;
        InputStreamReader streamReader = null;
        try {
            streamReader = new InputStreamReader(new FileInputStream(path + fileName), StandardCharsets.UTF_8);
            bf = new BufferedReader(streamReader); // 创建一个Buff对象 -- 缓冲区
            String line = bf.readLine(); // 初始化line
            while (line != null) {
                System.out.println(line);
                line = bf.readLine(); // 循环读取下一行
            }

        } catch (FileNotFoundException e) {
            System.out.println("文件不存在！");
//            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        } finally {
            try {
                if (streamReader != null)
                    streamReader.close();
                {
                }
                if (bf != null) {
                    bf.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Description: BufferedWriter 写入文本，写入文本通过Scanner输入
     *
     * @author: Berlin
     * @date: 21-8-10 1:24
     * @param:
     * @return:
     */
    public static void bufferWriterFile(String path, String fileName) {
        BufferedWriter bw = null;
        Writer writer = null;
        // 判断文件是否已经存在，如果存在则提示击中，反正则提示我即将新建
        File file = new File(path + fileName);
        if (!file.exists()) {
//            System.out.println(file.exists());
            System.out.println("文件不存在，已新建 ...");
        } else {
            System.out.println("--> 击中: " + fileName);
        }

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(path + fileName);
            writer = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
            boolean flag = true;
            while (flag) {
                System.out.println("请输入内容：");
                Scanner scanner = new Scanner(System.in);
                String content = scanner.nextLine();
                bw = new BufferedWriter(writer);
                bw.write(content);
                bw.flush();
                System.out.println("是否继续输入？[Y以继续，任意键退出 ...]:");
                flag = scanner.next().equals("Y");
            }
            System.out.println("当前文件内容: ");
            FileReaderTs(path, fileName);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

