import java.text.SimpleDateFormat;

/**
 * ClassName: TsLocalDate
 * Package: PACKAGE_NAME
 * Description:
 *
 * @date: 2021/8/6 1:59 下午
 * @author: 1412124522@qq.com
 */
public class TsLocalDate {
    public static void main(String[] args) {
        String sDate = "2021-10-10";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(sDate);
    }

}
