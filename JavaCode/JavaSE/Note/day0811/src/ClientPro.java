import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * ClassName:Client
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-11 0:48
 * @author: Berlin 1412124522@qq.com
 */
public class ClientPro {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        BufferedReader br = null;
        BufferedWriter bw = null;
        Socket sock = null;
        try {
            sock = new Socket("192.168.123.140", 408, null, 4099);  // 请求服务器，端口：7788 --> 服务端写入一行 欢迎语... 下面读取出欢迎语句
            InputStream input = sock.getInputStream();     // 创建输入流
            OutputStream output = sock.getOutputStream();  // 创建输出流
            bw = new BufferedWriter(new OutputStreamWriter(output, StandardCharsets.UTF_8)); // 带有缓冲区的写对象，后续向服务端写入语句
            br = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8));   // 带有缓冲区的读对象，后续读取服务端输出内容
            System.out.println("--> Server: [" + sock.getRemoteSocketAddress() + "] :" + br.readLine()); // 读取欢迎语 ...
            // 预设两个字符容器，预与服务端交涉
            String ipputContainer = "";  // 创建一个字符容器，预向服务端输送
            String outputContainer = ""; // 创建一个字符容器，接受服务端的输出
            // 输入exit()退出
            while (true) {
                // TODO 向服务端发送数据
                System.out.println("--> Client(Entry \"exit()\" to quit):"); // 提示语句
                ipputContainer = scanner.nextLine();
                bw.write(ipputContainer);
                bw.newLine();
                bw.flush();
                // TODO 接收服务端信息
                outputContainer = br.readLine();
                System.out.println("--> Server: [" + sock.getRemoteSocketAddress() + "]: " + outputContainer);
                // 如果服务器返回的是`Bye!See you next time ...`,则循环结束
                if ("Bye!See you next time ... ".equals(outputContainer)) {
                    System.out.println("Lost connection with " + sock.getRemoteSocketAddress() +"...");
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (bw != null) {
                    bw.close();
                }
                if (sock!=null) {
                    sock.close();
                }
                scanner.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
