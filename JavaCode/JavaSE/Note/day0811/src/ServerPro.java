import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

/**
 * ClassName:Service
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-11 0:48
 * @author: Berlin 1412124522@qq.com
 */
public class ServerPro {
    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        Socket accept = null;
        BufferedWriter bw = null;
        BufferedReader br = null;

        try {
            serverSocket = new ServerSocket(408); // 监听408端口 Target
            System.out.println("--> System message: Server: ["
                    + serverSocket.getInetAddress()
                    + "] is working. ");  // 监听程序已启动...
            while (true) {
                // 从连接请求队列中取出一个客户的连接请求，
                // 然后创建与客户连接的Socket对象，并将它返回。
                // 如果队列中没有连接请求，accept()方法就会一直等待，
                // 直到接收到了连接请求才返回。
                accept = serverSocket.accept(); // 接受用户连接请求
                System.out.println("--> System message: Request from : ["
                        + accept.getRemoteSocketAddress()
                        + "] succeed. "); // 接收到用户连接请求

                // 创建输出流输出欢迎语，防止乱码创建OutputStreamWriter
                OutputStreamWriter osw = new OutputStreamWriter(
                        accept.getOutputStream(),
                        StandardCharsets.UTF_8);
                // 以缓冲的方式，写入流
                bw = new BufferedWriter(osw);
                bw.write("  Welcome! Client "
                        + accept.getRemoteSocketAddress()
                        + "! ");
                bw.newLine(); // 指向下一行
                bw.flush(); // 刷新缓冲区
                String clientInput = ""; // 获取客户端输入
                InputStreamReader sr = new InputStreamReader(
                        accept.getInputStream(),
                        StandardCharsets.UTF_8);
                br = new BufferedReader(sr);
                while (true) {
                    try {
                        clientInput = br.readLine();
                    } catch (NullPointerException e) {
//                            e.printStackTrace();
                        System.out.println("Client enter nothing...");
                    }

                    if ("exit()".equals(clientInput)) {
                        bw.write("Bye!See you next time ... ");
                        bw.flush();
                        break;
                    }
                    System.out.println("--> Client :["
                            + accept.getRemoteSocketAddress()
                            + "] $ " + clientInput);
                    bw.write("Get Datas, But i want to the server returns unchanged datas --> " + clientInput);
                    bw.newLine();
                    bw.flush();

                }
                accept.close();

                if (accept.isClosed()) {
                    System.out.println("--> System message: Request from : ["
                            + accept.getRemoteSocketAddress()
                            + "]. Connection closed  ... ");
                }
            }
            // % [ /滑稽 ] %


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (bw != null) {
                    bw.close();
                }
                if (serverSocket != null) {
                    System.out.println("Server is offline ...\n");
                }
                if (accept != null) {
                    System.out.println("Client is offline ...\n");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
