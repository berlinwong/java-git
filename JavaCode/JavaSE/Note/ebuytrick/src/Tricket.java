/**
 * ClassName:Tricket
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-12 15:18
 * @author: Berlin 1412124522@qq.com
 */
public class Tricket extends Thread{
    private String destination; // 目的地
    private int margin; // 余量

    public Tricket(String destination, int margin) {
        this.destination = destination;
        this.margin = margin;
    }

    @Override
    public void run() {
        super.run();
        margin--;
        System.out.println(Thread.currentThread().getName() + margin);
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }
}
