/**
 * ClassName:EBuyTricket
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-12 15:35
 * @author: Berlin 1412124522@qq.com
 */
public class EBuyTricket {
    public static void main(String[] args) {
        tricketInit();
        Tricket sh = new Tricket("南京发上海", 26);
        Thread xc = new Thread(sh);
        xc.start();
        System.out.println(sh.getMargin());
    }

    public static void tricketInit() {
        Tricket sh = new Tricket("南京发上海", 26);
        Tricket hz = new Tricket("南京发杭州", 34);
        Tricket yz = new Tricket("南京发扬州", 15);
        System.out.printf("%-8s\t%s\n","始发地","余票");
        System.out.printf("%-8s\t%s\n",sh.getDestination(),sh.getMargin());
        System.out.printf("%-8s\t%s\n",hz.getDestination(),hz.getMargin());
        System.out.printf("%-8s\t%s\n",yz.getDestination(),yz.getMargin());
    }
}
