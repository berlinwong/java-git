import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

/**
 * ClassName:FilesReader
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-9 15:50
 * @author: Berlin 1412124522@qq.com
 */
public class FilesReader {
    public static ArrayList<FileItem> getList(String path) {
        File file = new File(path);
        ArrayList<FileItem> fileList = new ArrayList<>();
        String[] list = file.list();
        for (int i = 0; i < list.length; i++) {
            fileList.add(new FileItem());
            fileList.get(i).setFileName(list[i]);
            File fileChild = new File(path + "\\" + list[i]);
            long childLength = fileChild.length();
            fileList.get(i).setFileSize(childLength);
        }

        return fileList;
    }

    public static long bin2m(long num) {
        return num/(1024*1000);
    }
    /**
    * Description: 打印当前目录所有文件名
    * @author: Berlin
    * @date: 21-8-9 15:57
    * @param:
    * @return:
     * @param fileList
    */
    public  static void print(ArrayList<FileItem> fileList) {
        System.out.printf("    %-64s\t         %4s\n","文件名","大小");
        System.out.println("---------------------------------- 文件管理系统 ------------------------------------------------");
        for (FileItem file:fileList
        ) {
//            System.out.println(bin2m(file.getFileSize()));
            System.out.printf("%-64s\t ............%4dM\n",file.getFileName(),bin2m(file.getFileSize()));
        }
        System.out.println("---------------------------------- END ------------------------------------------------");

    }

    public static void main(String[] args) {
        ArrayList<FileItem> fileList = getList("D:\\Berlin\\Desktop");
        print(fileList);
    }
}
