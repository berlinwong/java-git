/**
 * ClassName:FileModel
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-9 16:01
 * @author: Berlin 1412124522@qq.com
 */
public class FileItem {
    private String fileName;
    private long fileSize;

    public String getFileName() {
        return fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
}
