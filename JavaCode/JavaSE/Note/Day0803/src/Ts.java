/**
 * @author Berlin
 * @date 21-8-3
 * @desc
 */
public class Ts {
    public static void main(String[] args) {
        WoodenDoor woodenDoor = new WoodenDoor();
        woodenDoor.setName("小花的房间");
        woodenDoor.setType("木门");
        woodenDoor.openLock();
        woodenDoor.open();

        woodenDoor.close();
        woodenDoor.lockerUp();
        woodenDoor.ring();

    }
}
