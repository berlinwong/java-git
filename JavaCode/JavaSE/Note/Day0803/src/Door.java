/**
 * @author Berlin
 * @date 21-8-3
 * @desc
 */
public abstract class Door {
    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public abstract void open();
    public abstract void close();

}
