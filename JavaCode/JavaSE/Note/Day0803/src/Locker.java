/**
 * @author Berlin
 * @date 21-8-3
 * @desc
 */
public interface Locker {
    void lockerUp();
    void openLock();
}
