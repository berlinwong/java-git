/**
 * @author Berlin
 * @date 21-8-3
 * @desc
 */
public class WoodenDoor extends Door implements Locker, Ringable{
    @Override
    public void open() {
        System.out.println(this.getName() + "开门了！");
    }

    @Override
    public void close() {
        System.out.println(this.getName() + "门关了！");

    }

    @Override
    public void lockerUp() {
        System.out.println(this.getName() + "钥匙正转一圈！锁上了");
    }

    @Override
    public void openLock() {
        System.out.println(this.getName() + "钥匙反转一圈，门开了");

    }

    @Override
    public void ring() {
        System.out.println(this.getName() + "叮叮叮，快开门！");

    }
}
