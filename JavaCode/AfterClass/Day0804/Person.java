package Day0804;

/**
 * ClassName:Demo
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-3 20:49
 * @author: Berlin 1412124522@qq.com
 */
public class Person {
    public Person() {
        System.out.println("执行Person()...");
    }
    public static int money = 500;
    public int id;

    static{
        System.out.println("执行static{}...");
        money+=500;
    }

    public static void main(String[] args) throws Exception {
        Class.forName("Day0804.Person");
        new Person();
        System.out.println(Person.money);
    }
}