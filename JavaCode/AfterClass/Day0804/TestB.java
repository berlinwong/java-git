package Day0804;

/**
 * ClassName:Day0804.TestB
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-3 19:32
 * @author: Berlin 1412124522@qq.com
 */
public class TestB {
    public static void main(String[] args) {
        TestA o = new TestA();
        new TestB().addOne(o);
    }
    public void addOne(final TestA testA) {
        testA.i++;
//        System.out.println(testA.i);
//        testA.i++ ;
//        System.out.println(testA.i);
    }
}

class TestA {
    public int i;
}
