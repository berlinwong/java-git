package Day0804D;

/**
 * ClassName:Operator
 * Package:Day0804D
 * Description:
 *
 * @date: 21-8-4 17:28
 * @author: Berlin 1412124522@qq.com
 */
public abstract class Operator {
    private String taste;
    private int price;

    /**
     * Description: 操作员操作
     *
     * @author: Berlin
     * @date: 21-8-4 17:30
     * @param:
     */
    public abstract void introduce();

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
