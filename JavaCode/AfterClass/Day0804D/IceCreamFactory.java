package Day0804D;

/**
 * ClassName:IceCreamInit
 * Package:Day0804D
 * Description:
 *
 * @date: 21-8-4 17:40
 * @author: Berlin 1412124522@qq.com
 */
public class IceCreamFactory {
    public static Operator createIcerea(int chooseNum) {
        Operator operator = null;
        switch (chooseNum) {
            case 1:
                operator = new StrawberryIceCream();
                break;
            case 2:
                operator = new BlueberryIceCream();
                break;
            default:
                System.out.println("对不起，没有该商品！");
        }

        return operator;
    }
}
