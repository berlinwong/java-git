package Day0804D;

import java.util.Scanner;

/**
 * ClassName:Client
 * Package:Day0804D
 * Description:
 *
 * @date: 21-8-4 17:32
 * @author: Berlin 1412124522@qq.com
 */
public class Client {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("***  冰淇淋自主点单  ***");
        System.out.println("1.草莓味 2.蓝莓味");
        System.out.println("请问你需要点个什么口味的冰淇淋呢？");
        int chooseNum = scanner.nextInt();
        Operator operator = IceCreamFactory.createIcerea(chooseNum);
        operator.introduce();
    }
}
