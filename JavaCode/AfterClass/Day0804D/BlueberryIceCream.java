package Day0804D;

/**
 * ClassName:BlueberryIceCream
 * Package:Day0804D
 * Description:
 *
 * @date: 21-8-4 17:58
 * @author: Berlin 1412124522@qq.com
 */
public class BlueberryIceCream extends Operator {
    @Override
    public void introduce() {
        this.setTaste("蓝莓味");
        this.setPrice(7);
        System.out.println(this.getTaste() + "冰淇淋，价格为" + this.getPrice() + "元，感谢您的下次光临！");
    }
}
