package Day0804D;

/**
 * ClassName:StrawberryIceCream
 * Package:Day0804D
 * Description:
 *
 * @date: 21-8-4 17:47
 * @author: Berlin 1412124522@qq.com
 */
public class StrawberryIceCream extends Operator {
    @Override
    public void introduce() {
        this.setTaste("草莓味");
        this.setPrice(6);
        System.out.println(this.getTaste() + "冰淇淋，价格为" + this.getPrice() + "元，感谢您的下次光临！");
    }
}
