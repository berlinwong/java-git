/**
 * ClassName:TsThread
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-12 19:13
 * @author: Berlin 1412124522@qq.com
 */
public class TsThread {
    public static void main(String[] args) {
        MoveThread moveThread = new MoveThread();
        Thread thread1 = new Thread(moveThread, "张三");
        Thread thread2 = new Thread(moveThread, "朋友一");
        Thread thread3 = new Thread(moveThread, "朋友二");
        Thread thread4 = new Thread(moveThread, "朋友三");
        Thread thread5 = new Thread(moveThread, "朋友四");
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();


    }
}
