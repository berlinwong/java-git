/**
 * ClassName:MoveThread
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-12 19:07
 * @author: Berlin 1412124522@qq.com
 */
public class MoveThread implements Runnable {
    public int totalBrick = 10;

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        while (totalBrick > 0) {
            synchronized (this) {
                totalBrick--;
                long endTime = System.currentTimeMillis();
                System.out.println(Thread.currentThread().getName() + "说，剩余：" + totalBrick);
                if (totalBrick == 0) {
                    System.out.println("下班，干饭人！");
                    System.out.println("程序运行时间：" + (endTime - startTime + 100) + "ms");
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
