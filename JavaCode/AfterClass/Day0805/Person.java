package Day0805;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ClassName:Person
 * Package:Day0805
 * Description:
 *
 * @date: 21-8-5 17:45
 * @author: Berlin 1412124522@qq.com
 */

public class Person {
    public String id;
    public String name;

    /**
     * Description: Person 构造器
     *
     * @author: Berlin
     * @date: 21-8-5 17:54
     * @param:
     * @return:
     */
    public Person(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public static void main(String[] args) {
        ArrayList<Person> people = newPerson();
        forPrintInIterator(people);
//        System.out.println(newPerson().get(2).name); // 获取第三个元素的name值
        System.out.printf("第二个元素：%s\n", people.get(2).name);

//        removeItem(newPerson()); // 删除最后一个元素
        removeItem(people);

//        forPrintInIterator(newPerson());
        forPrintInIterator(people);

        deletePlanA(people,"Tom");
        deletePlanB(people,"张一");


    }

    /**
     * Description:  创建person数组，保存五个人的基本id和name，返回 ArrayList<Person>
     *
     * @author: Berlin
     * @date: 21-8-5 17:53
     * @param:
     * @return: ArrayList<Person>
     */
    public static ArrayList<Person> newPerson() {
        ArrayList<Person> person = new ArrayList<>();
        person.add(new Person("0", "张零"));
        person.add(new Person("1", "张一"));
        person.add(new Person("2", "张二"));
        person.add(new Person("3", "Tom"));
        person.add(new Person("4", "张思"));
        return person;
    }


    /**
     * Description: 删除指定位置元素
     *
     * @author: Berlin
     * @date: 21-8-5 17:55
     * @param:
     * @return:
     */

    public static void removeItem(ArrayList<Person> person) {
        person.remove(person.size() - 1);
        System.out.println("Delete is OK！");
    }

    /**
     * Description: Iterator 和 foreach 遍历打印
     *
     * @author: Berlin
     * @date: 21-8-5 17:59
     * @param:
     * @return:
     */

    public static void forPrintInIterator(ArrayList<Person> person) {
        // TODO 最后分别使用Iterator和forEach遍历集合元素，显示每个元素的name值。
        // Iterator方式遍历
        // 获取迭代器
//        Iterator<Person> iterator = person.iterator();
//
        // 迭代
//        while(iterator.hasNext()){
//            Person item = iterator.next();
//            System.out.println(item.name);
//        }

        // Description: 使用foreach遍历
        for (Person item : person) {
            System.out.println(item.name);
        }

        System.out.printf("一共有%d个元素\n\n",person.size());

    }

    /**
    * Description: 使用两种方法删除上述集合中name值为Tom的元素。
    * @author: Berlin
    * @date: 21-8-5 18:13
    * @param:
    * @return:
    */

    // 使用for 遍历并比对取出下标，再删除
    public static void deletePlanA(ArrayList<Person> person,String name) {
        for (int i = 0; i < person.size(); i++) {
            if (person.get(i).name.equals(name)) {
                person.remove(person.get(i));
                System.out.println("已删除:" + name);
                forPrintInIterator(person);
            }
        }
    }

    // 使用泛型，取出name进行比对，再使用remove删除
    public static void  deletePlanB(ArrayList<Person> person,String name) {
        Iterator<Person> iterator = person.iterator();
        while(iterator.hasNext()){
            Person item = iterator.next();
            if (item.name.equals(name)) {
                iterator.remove();
            }
        }
        forPrintInIterator(person);


    }


}
