package Day0805;

import java.util.*;

/**
 * ClassName:RndInt
 * Package:Day0805
 * Description: 产生10个1-20之间的随机数，要求随机数不能重复
 *
 * @date: 21-8-5 18:54
 * @author: Berlin 1412124522@qq.com
 */
public class RndInt {
    //TODO 产生10个1-20之间的随机数，要求随机数不能重复（List 和 Set 分别实现）
    public static void main(String[] args) {
        rndIntPlanA(1, 20, 10);
        rndIntPlanB(1, 20, 10);
    }
/**
* Description: 使用 Arraylist 生成随机数
* @author: Berlin
* @date: 21-8-5 19:09
* @param:
* @return: ArrayList<Integer> intCreated
*/

    public static void rndIntPlanA(int min, int max, int count) {
        ArrayList<Integer> intPool = new ArrayList<>();
        ArrayList<Integer> intCreated = new ArrayList<>();
        for (int i = min; i < max + 1; i++) {
            intPool.add(i);
        }
        System.out.println("常量池：" + intPool);
        for (int j = 0; j < count; j++) {
            int getNum = (int) (Math.random() * (intPool.size()));
            intCreated.add(intPool.get(getNum));
            intPool.remove(getNum);
        }
        System.out.println(intCreated);
    }

    // 利用HashSet的元素唯一特性
    public static void rndIntPlanB(int min, int max, int count) {
        HashSet<Integer> set = new HashSet<>();
        do {
            int getNum = (int) ((Math.random() * max - min + 1) + min);
            set.add(getNum);
        } while (set.size() < count);
        System.out.println(set);
    }

}
