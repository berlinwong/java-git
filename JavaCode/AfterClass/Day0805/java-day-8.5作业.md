# java-day-9.5作业

###### 1. 有一个`Person`类，要求使用`ArrayList`保存5个对象元素，获取并第三个元素的name值, 删除最后一个元素，最后分别使用`Iterator`和`forEach`遍历集合元素，显示每个元素的name值。

   ```
   public class Person{
   	public String id;
   	public String name;
   }
   ```

###### 2. 使用两种方法删除上述集合中`name`值为`Tom`的元素。

###### 3. 产生10个1-20之间的随机数，要求随机数不能重复（`List` 和 `Set`分别实现）

###### 4. HashMap与HashTable的区别?
```
HashMap是基于哈希表实现的，每一个元素是一个key-value对，其内部通过单链表解决冲突问题，容量不足时，同样会自动增长
HashMap和HashTable都使用哈希表来存储键值对。在数据结构上是基本相同的，都创建了一个继承自Map.Entry的私有的内部类Entry，每一个Entry对象表示存储在哈希表中的一个键值对。
Hashtable继承自Dictionary类，而HashMap继承自AbstractMap类。但二者都实现了Map接口。
```

###### 5. `ArrayList list=new ArrayList(20);`中的list扩容几次（  A   ）
  ```
  A. 0
  B. 1
  C. 2
  D. 3
  ```
   * 此处的`ArrayList(20)`已经给定初始长度，虽然`list`可以扩容，但是在本条语句中，初始创建容量为`20`的`list`。故未扩容，答案为`0`;

###### 6. List、Set、Map哪个继承自Collection接口（   C  ）
```
   A. List Map
   B. Set Map
   C. List Set
   D. List Map Set
```

###### 7. 简述ArrayList的扩容机制，最好能贴图说明。


以无参数构造方式创建ArrayList时，初始化赋值的是一个空数组，
例如本次作业过程中的`ArrayList<Person> person = new ArrayList<>();`
当真正对数组进行添加元素操作时，例如 person.add(new Person("0", "张零")); 才真正分配容量。
![img.png](img.png)
首先分析ArrayList的Add函数，这个地方写的啥也没看懂，那就接着看看`ensureCapacityInternal`，emm 也没看出什么东西，预测是给后面的元素添了个地儿。

```java
    private void ensureCapacityInternal(int minCapacity) {
        ensureExplicitCapacity(calculateCapacity(elementData, minCapacity));
    }

```

总结：ArrayList是采取延迟分配对象数组大小空间的，当第一次添加元素时才会分配10个对象空间，当添加第11个元素的时候，会扩容1.5倍，当添加到16个元素的时候扩容为15*1.5=22，以此类推


###### 8. 小明家新装修了，现在需要一批家电：电灯、电视、冰箱、空调、洗衣机。小明喜欢TCL品牌的，所以买的都是该品牌的。后来，小明娶了老婆，老婆喜欢海尔品牌的，小明家有钱，任性，现在需要将所有的家电换成海尔品牌。用抽象工厂进行描述。

![img_1.png](img_1.png)