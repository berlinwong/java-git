1.HashMap的底层数据结构是什么？
```text
jdk1.7中，由数组+链表组成，数组是hashmap的主体，链表主要是为了解决哈希冲突而存在的
在jdk1.8中，由数组+链表+红黑树组成。
```

2.HashMap中的链表，什么时候会转为红黑树？为什么要转为红黑树？
```text
当链表长度超过8时，且数组长度超过64时，当前链表会转为红黑树。
红黑树搜索时间复杂度是 O(logn)，而链表是 O(n)，此时需要红黑树来加
快查询速度,但是新增节点的效率变慢了。
```

3.什么是hash碰撞？如何解决hash碰撞？发生hash碰撞时，如何存储数据？
```text
元素索引位置和hash值完全一致，此时产生hash碰撞。
1.8之前，数据链表解决，1.8之后，使用链表+红黑树
若一致，则后添加的value值，就覆盖之前的value值。
若不一致，那么系统继续与该链表的下个节点的key进行比较，若hash值与key的内容都不相等，则
再往下划分一个新的节点存储数据。

```
4.为什么集合的初始容量是16，且容量必须是2的n次幂？
```text
HashMap为了存取高效，要尽量减少hash碰撞，就是要尽量把数据分配均匀，
每个链表长度大致相同，这个实现使用了一种在把数据存到哪个链表中的算法---取模（hash%length）。
然而，计算机中直接求余效率不如位移运算。所以源码中做了优化,使用 hash&(length-1)，而实际上让
hash%length等于hash&(length-1)的前提是length是2的n次幂。
```


5.默认加载因子是多少? 假设当前hashmap容量为50，当前hashmap中元素实时数量超过多少时会扩容？
```text
默认加载因子是0.75
threshold = length * Load factor 
threshold = 64 * 0.75 = 48
```


6.假设要存储的数据个数为500个，则hashmap的容量初始值设置多少合适？
```text
initialCapacity = (需要存储的元素个数 / 负载因子) + 1 
500 / 0.75 +1 = 667
```

7.简述使用jdbc操作数据库的步骤。Connection的作用是什么？
```java
// 加载驱动
Class.forName("oracle.jdbc.driver.OracleDriver");

//  建立连接
String dbURL = "jdbc:oracle:thin:@localhost:1521:helowin";
Connection conn = DriverManager.getConnection(dbURL, "scott", "123456");
System.out.println("连接成功");

//捕获异常
//关闭连接
conn.close();

与特定数据库的连接（会话）。 执行SQL语句并在连接的上下文中返回结果。
Connection对象的数据库能够提供描述其表，其支持的SQL语法，其存储过程，此连接的功能等的信息。 该信息是用getMetaData方法获得的。
```


