import java.io.*;

/**
 * ClassName:IODemo
 * Package:PACKAGE_NAME
 * Description:
 *
 * @date: 21-8-9 17:13
 * @author: Berlin 1412124522@qq.com
 */
public class IODemo {
    public static void copyIO() throws IOException {
        File afile = new File("D:\\Berlin\\Desktop\\java-git\\JavaCode\\AfterClass\\day0809\\fileTest\\a.txt");
        File bfile = new File("D:\\Berlin\\Desktop\\java-git\\JavaCode\\AfterClass\\day0809\\fileTest\\b.txt");
        FileInputStream fileFromA = new FileInputStream(afile);
        FileOutputStream file2b = new FileOutputStream(bfile);
        byte[] count = new byte[fileFromA.available()];
        int len;
        while ((len = fileFromA.read(count))!= -1) {
            file2b.write(count,0,len);
            file2b.flush();
        }
        fileFromA.close();
        file2b.close();
    }

    public static void main(String[] args) throws IOException {
        copyIO();
    }
}
